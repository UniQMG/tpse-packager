const { createCanvas, loadImage, Image } = require('canvas');
const svg2img = require('svg2img');
const fetch = require('node-fetch');
const fs = require('fs');

const template = fs.readFileSync('./resources/template.svg', 'utf8');

module.exports = async function fetchSkin(url) {
  let data = {};

  if (url.endsWith('svg')) {
    let request = await fetch(url);
    if (!request.ok) throw new Error('request failed');
    data.skin = await request.text();

    let image = await new Promise((res, rej) => {
      svg2img(data.skin, { width: 372, height: 30 }, (error, buffer) => {
        if (error) rej(error);
        res(buffer);
      });
    });
    data.skinPng = 'data:image/svg+xml;base64,'+image.toString('base64');
  } else {
    let image = await loadImage(url);
    const canvas = createCanvas(image.width, image.height);
    canvas.getContext('2d').drawImage(image, 0, 0, image.width, image.height);
    data.skinPng = canvas.toDataURL('image/png');
    data.skin = template.replace('<!--custom-image-embed-->', data.skinPng);
  }

  return data;
}

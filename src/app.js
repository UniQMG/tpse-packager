const express = require('express');
const morgan = require('morgan');
const app = express();

const fetchSkin = require('./fetchers/skin');
const URL = require('whatwg-url').URL;

app.use(morgan(
  `:remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length]`
));
app.get(/\/([\w-_]+).tpse/, async (req, res) => {
  const tpse = {};

  try {
    if (req.query.skin) {
      let url = new URL(req.query.skin);
      if (url.origin != 'https://you.have.fail')
        return res.sendStatus(400);
      Object.assign(tpse, await fetchSkin(url.toString()));
    }
  } catch(ex) {
    console.log(ex);
    return res.sendStatus(400);
  }

  res.set('Access-Control-Allow-Origin', '*');
  res.send(tpse);
});

app.listen(8089);

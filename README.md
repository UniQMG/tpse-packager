# TPSE Packager

Simple node server that packages remote resources into .tpse files

## Routes

GET \*.tpse
PARAMS
  skin=<image url at https://you.hvae.fail>

Parameters should be uriencoded
